﻿using System;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace TrayFolder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region VARIABLES
        private KeyboardHook kbHook;
        private NotifyIcon trayIcon;
        System.Windows.Controls.Primitives.UniformGrid contentGrid;

        private string startupPath;
        private string currentPath;

        private System.Drawing.Point lastKnownMousePos;

        private readonly int MaxCols = 10;
        private readonly int MaxRows = 10;
        #endregion // VARIABLES

        #region FORM RELATED
        public MainWindow()
        {
            startupPath = AppDomain.CurrentDomain.BaseDirectory + "autoload";

            // NOTIFY ICON
            // create notify icon and context menu
            trayIcon = new NotifyIcon();
            trayIcon.ContextMenu = new ContextMenu();
            MenuItem exitItem = new MenuItem("Exit");
            // set notify icon image
            Assembly TempAssembly = GetType().Assembly;
            trayIcon.Icon = System.Drawing.Icon.FromHandle(new Bitmap(TempAssembly.GetManifestResourceStream(TempAssembly.GetName().Name + ".TrayFolder.png")).GetHicon());
            // add event handlers
            trayIcon.MouseDown += (o, e) => {
                if (e.Button == MouseButtons.Left)
                    if (LoadFolder(startupPath))
                        ShowFolder(startupPath);
            };
            trayIcon.MouseMove += (o, e) => { lastKnownMousePos = Control.MousePosition; };
            exitItem.Click += (o, e) => { Close(); };
            // add context menu items
            trayIcon.ContextMenu.MenuItems.Add(exitItem);

            // MAIN FORM EVENTS
            Deactivated += (o, e) =>
            {
                Hide();
                files.Items.Clear();
                GC.Collect();
            };
            Closing += (o, e) =>
            {
                trayIcon.Dispose();
                files.Items.Clear();
                GC.Collect();
            };
            PreviewKeyDown += (o, e) =>
            {
                switch (e.Key)
                {
                    case Key.Enter:     OpenSelectedItem();                 break;
                    case Key.Back:      OpenParentFolder();                 break;
                    case Key.Space:     OpenCurrentFolder();                break;
                    case Key.Escape:    OnDeactivated(null);                break;
                    default:            SelectItemStartingWith(""+e.Key);   break;
                }
            };
            
            InitializeComponent();
            trayIcon.Visible = true;

            // keyboard hook
            kbHook = new KeyboardHook();
            kbHook.KeyPressed += new EventHandler<KeyPressedEventArgs>((o, e) => { if (LoadFolder(startupPath)) ShowFolder(startupPath); });
            kbHook.RegisterHotKey(ModifierKeys.Shift | ModifierKeys.Win, Keys.LWin);
        }

        private void ListBoxItem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (e.ChangedButton)
            {
                case MouseButton.Left:
                    if (e.ClickCount > 1)
                        OpenSelectedItem();
                    break;
                case MouseButton.Middle:
                    OpenCurrentFolder();
                    break;
                case MouseButton.Right:
                    OpenParentFolder();
                    break;
            }
        }

        private void UniformGrid_Loaded(object sender, RoutedEventArgs e)
        { contentGrid = sender as System.Windows.Controls.Primitives.UniformGrid; }
        
        #region ITEM OPERATIONS
        public void OpenSelectedItem()
        {
            FileItem item = files.SelectedItem as FileItem;
            if (item != null)
            {
                FileAttributes attr = File.GetAttributes(item.Path);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    if (LoadFolder(item.Path))
                        ShowFolder(item.Path);
                }
                else
                    System.Diagnostics.Process.Start(item.Path);
            }
            item = null;
        }

        public void OpenCurrentFolder()
        {
            System.Diagnostics.Process.Start(currentPath);
        }

        public void OpenParentFolder()
        {
            if (currentPath != startupPath)
            {
                string parent = Path.GetDirectoryName(currentPath);
                if (LoadFolder(parent))
                    ShowFolder(parent);
            } else
                OnDeactivated(null);
        }

        public void SelectItemStartingWith(string key)
        {
            // exit if empty list
            if (files.Items.IsEmpty) return;
            
            int found = -1;
            // search from current selected item
            for (int i = files.SelectedIndex + 1; i < files.Items.Count; i++)
            {
                var item = files.Items.GetItemAt(i) as FileItem;
                if (item.Name.StartsWith(key) || item.Name.StartsWith(key.ToUpper()) || item.Name.StartsWith(key.ToLower()))
                {
                    found = i;
                    break;
                }
            }
            // if not found from current selected then search from beggining
            if (found == -1)
            {
                for (int i = 0; i < files.Items.Count; i++)
                {
                    var item = files.Items.GetItemAt(i) as FileItem;
                    item = files.Items.GetItemAt(i) as FileItem;
                    if (item.Name.StartsWith(key) || item.Name.StartsWith(key.ToUpper()) || item.Name.StartsWith(key.ToLower()))
                    {
                        found = i;
                        break;
                    }
                }
            }
            // again check if found
            if (found > -1)
            {
                files.SelectedIndex = found;
            }
        }
        #endregion // ITEM OPERATIONS
        #endregion // FORM RELATED

        #region ITEMS RELATED
        #region ITEM CLASS
        public class FileItem : IDisposable
        {
            public BitmapSource Icon { get; set; }
            public string Name { get; set; }
            public string Path { get; set; }

            public static readonly string[] excludedExtensions = { ".lnk", ".url" };

            public FileItem(string path)
            {
                Icon = ShellIcon.GetIcon(path).Clone();
                Path = path;
                Name = System.IO.Path.GetFileName(path);
                // Process Name to display
                if (Name.StartsWith("#"))
                    Name = Name.Substring(Name.IndexOf('.')+1);
                foreach (string extension in excludedExtensions)
                    Name = Name.Replace(extension, "");
            }

            public void Dispose()
            {
                Icon = null;
                Path = null;
                Name = null;
            }
        }
        #endregion // ITEM CLASS
        
        #region FOLDER LOADING
        private bool FileCheck(string path)
        {
            bool result = false;
            result |= path.EndsWith("desktop.ini");
            result |= path.EndsWith("thumbs.db");
            return !result;
        }

        public bool LoadFolder(string path)
        {
            System.Collections.Generic.List<string> newFiles = new System.Collections.Generic.List<string>();
            DirectoryInfo folder = new DirectoryInfo(path);
            foreach (DirectoryInfo dir in folder.GetDirectories())
                if (FileCheck(dir.FullName))
                    newFiles.Add(dir.FullName);
            foreach (FileInfo file in folder.GetFiles())
                if (FileCheck(file.FullName))
                    newFiles.Add(file.FullName);
            folder = null;
            if (newFiles.Count > 0)
            {
                files.Items.Clear();
                foreach (string file in newFiles)
                    files.Items.Add(new FileItem(file));
                newFiles.Clear();
                return true;
            }
            return false;
        }
        #endregion // FOLDER LOADING

        #region FOLDER SHOWING
        private void ShowFolder(string path)
        {
            currentPath = path;
            Show();
            if (contentGrid != null)
            { // set grid cols and rows based on items count
                contentGrid.Columns = files.Items.Count;
                if (contentGrid.Columns > MaxCols)
                    contentGrid.Columns = MaxCols;
                contentGrid.Rows = (files.Items.Count / MaxCols) + (files.Items.Count % MaxCols > 0 ? 1 : 0);
                if (contentGrid.Rows > MaxRows)
                    contentGrid.Rows = MaxRows;
            }
            ApplyTemplate();
            UpdateLayout();

            System.Drawing.Point location = lastKnownMousePos;
            { // SET POSITION
                Rect? trayRect = WindowPositioning.GetNotifyIconRectangle(trayIcon);
                if (trayRect != null)
                {
                    location.X = (int)(trayRect.Value.Left + (trayRect.Value.Width / 2));
                    location.Y = (int)(trayRect.Value.Top + (trayRect.Value.Height / 2));
                }
                trayRect = null;

                location.X -= (int)ActualWidth / 2;
                location.Y -= (int)ActualHeight;

                Func<Rectangle> workingArea = () => Screen.PrimaryScreen.WorkingArea;

                // check horizontal coordinates
                if (location.X < workingArea().X)
                    location.X = workingArea().X;
                if (location.X + Width > workingArea().X + workingArea().Width)
                    location.X = (int)(workingArea().X + workingArea().Width - Width);

                // check vertical coordinates
                if (location.Y < workingArea().Y)
                    location.Y = workingArea().Y;
                if (location.Y + Height > workingArea().Y + workingArea().Height)
                    location.Y = (int)(workingArea().Y + workingArea().Height - Height);

                workingArea = null;
            }
            Left = location.X;
            Top = location.Y;

            Activate();
            if (files.Items.Count > 0)
            {
                files.SelectedIndex = 0;
                files.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
            }
        }
        #endregion // FOLDER SHOWING
        #endregion // ITEMS RELATED
    }
}
