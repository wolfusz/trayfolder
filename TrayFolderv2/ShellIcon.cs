﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Media.Imaging;

namespace TrayFolder
{
    class ShellIcon
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        }

        private class Win32
        {
            [DllImport("shell32.dll")]
            public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

            [DllImport("User32.dll")]
            public static extern int DestroyIcon(IntPtr hIcon);
        }

        public static BitmapSource GetIcon(string fileName)
        {
            SHFILEINFO shinfo = new SHFILEINFO();
            IntPtr hImgSmall = Win32.SHGetFileInfo(fileName, 0x80, ref shinfo, (uint)Marshal.SizeOf(shinfo), 0x000000100);
            Icon sysicon = (Icon)Icon.FromHandle(shinfo.hIcon).Clone();
            BitmapSource icon = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(
                                      sysicon.Handle,
                                      System.Windows.Int32Rect.Empty,
                                      BitmapSizeOptions.FromEmptyOptions());
            Win32.DestroyIcon(shinfo.hIcon);
            Win32.DestroyIcon(sysicon.Handle);
            sysicon.Dispose();
            return icon;
        }
    }
}
